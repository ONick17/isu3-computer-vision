import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as ndimage
from skimage.measure import label

fgrs = np.array([np.array(([1, 1, 1, 1, 1, 1],
                           [1, 1, 1, 1, 1, 1],
                           [1, 1, 1, 1, 1, 1],
                           [1, 1, 1, 1, 1, 1],)),

                 np.array(([1, 1, 1, 1],
                           [1, 1, 1, 1],
                           [1, 1, 0, 0],
                           [1, 1, 0, 0],
                           [1, 1, 1, 1],
                           [1, 1, 1, 1],)),

                 np.array(([1, 1, 1, 1],
                           [1, 1, 1, 1],
                           [0, 0, 1, 1],
                           [0, 0, 1, 1],
                           [1, 1, 1, 1],
                           [1, 1, 1, 1],)),

                 np.array(([1, 1, 1, 1, 1, 1],
                           [1, 1, 1, 1, 1, 1],
                           [1, 1, 0, 0, 1, 1],
                           [1, 1, 0, 0, 1, 1],)),

                 np.array(([1, 1, 0, 0, 1, 1],
                           [1, 1, 0, 0, 1, 1],
                           [1, 1, 1, 1, 1, 1],
                           [1, 1, 1, 1, 1, 1],))], dtype=object)

img = np.load("ps.npy.txt")
#plt.imshow(img)
#plt.show()
ans = []
ans.append(label(ndimage.binary_erosion(img, fgrs[0])).max())

for i in range(1, len(fgrs)):
    img2 = label(ndimage.binary_erosion(img, fgrs[i]))
    if i in [3, 4]:
        ans.append(img2.max() - ans[0])
    else:
        ans.append(img2.max())

print("Total:", np.array(ans).sum())
print("Rectangles:", ans[0])
print("C-right:", ans[1])
print("C-left:", ans[2])
print("C-down:", ans[3])
print("C-up:", ans[4])
