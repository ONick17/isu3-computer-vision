import cv2

ans = 0
chk = True
for i in range(1, 13):
    cnt = 0
    img = cv2.imread("img ("+str(i)+").jpg")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gaussian = cv2.GaussianBlur(gray, (215,235), 0)
    thresh = cv2.threshold(gaussian, 140, 190, 0)[1]
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    cv2.drawContours(img, contours, -1, (0,255,0),0)

    areas = []
    for cntr in contours:
        if chk:
            chk = False
            continue
        moments = cv2.moments(cntr)
        x = int(moments["m10"] / moments["m00"])
        y = int(moments["m01"] / moments["m00"])
        area = cv2.contourArea(cntr, True)
        areas.append(area)

    for area in areas:
        if (area < 570000) and (area > 340000):
            cnt += 1

    print(f"Изображение №", i, ": \t", cnt, sep="")
    ans += cnt
print("Всего:", ans)
