import numpy as np
import matplotlib.pyplot as plt
from typing import DefaultDict
from skimage.measure import label, regionprops
from skimage import color



def mergeColors(cls):
    ans = cls.copy()
    keys = sorted(ans.keys())
    chk = np.std(np.diff(keys)) * 2
    for i in range(1, len(keys)):
        df = keys[i] - keys[i-1]
        if(df < chk):
            ans[keys[i]] += ans[keys[i-1]]
            ans.pop(keys[i-1], None)
    return dict(ans)



img = plt.imread('image.png')
img_hsv = color.rgb2hsv(img)[:, :, 0]
img_bin = np.mean(img, 2)
img_bin[img_bin > 0] = 1
img_label = label(img_bin)
props = regionprops(img_label)

rects = DefaultDict(lambda: 0)
circles = DefaultDict(lambda: 0)
for i in range(0, img_label.max()):
    y, x = props[i].centroid
    cl = img_hsv[int(y), int(x)]
    if (props[i].eccentricity):
        rects[cl] += 1
    else:
        circles[cl] += 1
rects = mergeColors(rects)
circles = mergeColors(circles)

print("Rectangles")
print("Sum:", sum(rects.values()))
print("By colors:")
for i in rects.keys():
    print(i, ":\t", rects[i], sep="")
print()
print("Circles")
print("Sum:", sum(circles.values()))
print("By colors:")
for i in circles.keys():
    print(i, ":\t", circles[i], sep="")
