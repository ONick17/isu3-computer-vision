import cv2
import numpy as np
import matplotlib.pyplot as plt

rose = cv2.imread("rose.jpg")
hsv = cv2.cvtColor(rose, cv2.COLOR_BGR2HSV)

lower = np.array([0, 200, 100])
upper = np.array([0, 255, 255])

mask = cv2.inRange(hsv, lower, upper)

plt.imshow(mask)
plt.show()

