import cv2
import numpy as np

cam = cv2.VideoCapture(0)
ret, frame_old = cam.read()
while cam.isOpened():
    frame_old = cv2.cvtColor(frame_old, cv2.COLOR_BGR2GRAY)
    ret, frame = cam.read()	
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    diff = cv2.absdiff(frame_old, frame_gray)
    diff = cv2.GaussianBlur(diff, (151, 151), 0)
    
    thresh = cv2.threshold(diff, 10, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, 
                                          cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        (x,y,w,h) = cv2.boundingRect(c)
        cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
    cv2.putText(frame, f"Differences = {len(contours)}", (10,30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0))
    cv2.imshow("Dif", frame)
    key = cv2.waitKey(10)
    if key > 0:
        if chr(key) == 'd':
            break
    ret, frame_old = cam.read()
cv2.destroyAllWindows()
