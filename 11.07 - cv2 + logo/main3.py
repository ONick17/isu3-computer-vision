import cv2
import numpy as np

cam = cv2.VideoCapture(0)
news = cv2.imread("news.jpg")
rows = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
cols = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))

pts_cam = np.float32([[0,0], [0, rows], [cols,0], [cols,rows]])
pts_news = np.float32([[19,25], [40, 294], [434,54],[432,270]]) 
M = cv2.getPerspectiveTransform(pts_cam, pts_news)

_, frame = cam.read()

aff = cv2.warpPerspective(frame, M, (news.shape[1], news.shape[0]))

pos = np.where(aff)
news[pos] = aff[pos]

while cam.isOpened():
    ret, frame = cam.read()
    aff = cv2.warpPerspective(frame, M, (news.shape[1], news.shape[0]))
    pos = np.where(aff)
    news[pos] = aff[pos]
    cv2.imshow("News", news)
    key = cv2.waitKey(10)
    if key > 0:
        if chr(key) == 'd':
            break
cv2.destroyAllWindows()
