import cv2
import numpy as np

cv2.namedWindow("Image", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_EXPOSURE, -5)
width = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
res = width / height

logo = cv2.imread("cvlogo.png", cv2.IMREAD_UNCHANGED)
logo_width = int(logo.shape[0] // 4)
logo_height = int(logo.shape[1] // 4 * res)
logo = cv2.resize(logo, (logo_width, logo_height))
mask = np.where(logo[:, :, -1])
logo = logo[:, :, :-1]
fl_logo = False

while cam.isOpened():
    ret, frame = cam.read()
    if fl_logo:
        frame[:logo_width, :logo_height][mask] = logo[mask]
    cv2.imshow("Image", frame)
    key = cv2.waitKey(10)
    if key > 0:
        if chr(key) == 'd':
            break
        elif chr(key) == ' ':
            fl_logo = not fl_logo
cv2.destroyAllWindows()
