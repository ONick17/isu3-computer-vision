import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import face
import copy
from skimage.transform import resize

#находит среднее значение пикселя с заданным односторонним шагом
def avblock(image, blockX, blockY):
    result = []
    for i in range(blockY, len(image), blockY*2+1):
        result.append([])
        for j in range(blockX, len(image[i]), blockX*2+1):
            #print(i, j, ":", end=' ')
            av = image[i][j]
            av_div = blockX*2+blockY*2+1
            if (blockY > 0):
                for keke in range(-blockY, blockY+1):
                    i2 = i+keke
                    if (i2>=len(image)):
                        i2 = image[i-len(image)+keke][j]
                    else:
                        i2 = image[i2][j]
                    #print(i2, end=' ')
                    av += i2
                av -= image[i][j]
            if (blockX > 0):
                for keke in range(-blockX, blockX+1):
                    j2 = j+keke
                    if (j2>=len(image[i])):
                        j2 = image[i][j-len(image[i])+keke]
                    else:
                        j2 = image[i][j2]
                    #print(j2, end=' ')
                    av += j2
                av -= image[i][j]
            #print("!!!", i, j, av, av_div)
            result[-1].append(av//av_div)
    return result

#меняет размер изображения на указанный
def avblock2(image, sizeX, sizeY):
    result = np.zeros((sizeY, sizeX), dtype=int)
    lenX = len(image[0])
    lenY = len(image)
    delX = lenX/sizeX
    delY = lenY/sizeY
    blockXr = round(1/delX)
    blockYd = round(1/delY)
    if (blockXr<1):
        blockXr = 1
    if (blockYd<1):
        blockYd = 1
    blockXr = blockXr//2+blockXr%2
    blockXl = -(blockXr//2)
    blockYd = blockYd//2+blockYd%2
    blockYu = -(blockYd//2)
    for i in range(sizeY):
        orI = round(i*delY)
        for j in range(sizeX):
            orJ = round(j*delX)
            av = image[orI][orJ]
            cnt = 1
            for keke in range(blockXl, blockXr+1):
                J = orJ + keke
                if (J<0) or (keke==0) or (J>=lenX):
                    continue
                cnt += 1
                av += image[orI][J]
            for keke in range(blockYu, blockYd+1):
                I = orI + keke
                if (I<0) or (keke==0) or (I>=lenY):
                    continue
                cnt += 1
                av += image[I][orJ]
            av = round(av/cnt)
            result[i][j] = av
    return result
    '''
    result = copy.deepcopy(image)
    result = resize(result, (sizeX, sizeY))
    return(result)
    '''


image = face(gray=True).astype(int)
image_new = avblock(image, 0, 1)
image_new2 = avblock2(image, 80, 100)
print(len(image))
print(len(image[0]))
print()
print(len(image_new))
print(len(image_new[0]))
print()
print(len(image_new2))
print(len(image_new2[0]))

plt.subplot(131)
plt.imshow(image)
plt.subplot(132)
plt.imshow(image_new)
plt.subplot(133)
plt.imshow(image_new2)
plt.show()
'''
image = [[0, 1, 0, 0, 0, 3],
         [1, 1, 0, 0, 3, 0],
         [0, 0, 0, 3, 3, 3],
         [0, 0, 0, 0, 3, 0],
         [0, 0, 0, 0, 0, 2],
         [4, 0, 0, 0, 1, 0]]
image_new = avblock(image, 1)
for i in image:
    print(i)
print()
for i in image_new:
    print(i)
'''
