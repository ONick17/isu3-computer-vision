from matplotlib import image
import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import morphology
from skimage.measure import label, regionprops
from skimage.filters import try_all_threshold, threshold_triangle, threshold_otsu
from collections import defaultdict



def count_lakes_and_bays(prop):
    b = ~prop.image
    bb = np.ones((b.shape[0] + 2, b.shape[1] + 2))
    bb[1:-1, 1:-1] = b
    count_lakes = np.max(label(bb)) - 1
    count_bays = np.max(label(b))
    return count_lakes, count_bays

def has_line(prop, keke):
    return (1 in (np.sum(prop.image, keke) // prop.image.shape[keke]))

def has_bay(prop):
    b = ~prop.image
    bb = np.zeros((b.shape[0] + 1, b.shape[1])).astype("uint8")
    bb[:-1, :] = b
    b = bb
    bb = np.ones((b.shape[0] + 2, b.shape[1] + 2))
    bb[1:-1, 1:-1] = b
    return np.max(label(bb)) - 2

def find_d(prop):
    #cy,cx = prop.centroid
    #d = ((cy-prop.image.shape[0]//2)**2 +(cx-prop.image.shape[0]//2)**2 )**0.5
    #d = cy / prop.image.shape[0]
    d = prop.perimeter ** 2 / prop.area
    return d

def recognize(image):
    result = defaultdict(lambda: 0)
    labeled = label(image)
    props = regionprops(labeled)
    for prop in props:
        lakes, bays = count_lakes_and_bays(prop)  
        if np.all(prop.image): # -
            result["-"] += 1
        elif lakes == 2: # B, 8
            if (bays <= 4) and has_line(prop, 0):
                result["B"] += 1
            else:
                result["8"] += 1
        elif lakes == 1: # A, P, D, 0
            if has_bay(prop) > 0:
                result["A"] += 1
            elif (bays == 3) and has_line(prop, 0):
                if find_d(prop) > 58:
                    result["D"] += 1
                else:
                    result["P"] += 1
            else:
                result["0"] += 1
        elif lakes == 0: # 1, /, X, *, W
            if (bays == 3) and has_line(prop, 0):
                result["1"] += 1
            elif bays == 2:
                result["/"] += 1
            elif bays == 5:
                if (find_d(prop) < 50) and has_line(prop, 1):
                    result["*"] += 1
                else:
                    result["W"] += 1
            else:
                if (bays == 4) and (find_d(prop) > 40):
                    result["X"] += 1
                else:
                    result["*"] += 1
        else:
            result["unknown"] += 1
    return result



im = plt.imread("symbols.png")
im = np.mean(im, 2)
im[im > 0] = 1
rec = recognize(im)

print("Total: ", sum(rec.values()))
for i in rec.keys():
    percent = rec.get(i) / sum(rec.values()) * 100
    print(i, ": \t percent = ", percent, "%, \t count = ", rec[i], sep="")
