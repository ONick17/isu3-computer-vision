import matplotlib.pyplot as plt
import numpy as np
from skimage.exposure import histogram
from skimage.filters import (threshold_otsu,
                             threshold_local,
                             threshold_li,
                             threshold_yen)
from skimage.filters import gaussian


img = plt.imread("coins.jpg")
img = np.mean(img, 2).astype("uint8")

'''
th1 = img.copy() > threshold_local(img, 101)
th2 = img.copy() > threshold_otsu(img)
th3 = img.copy() > threshold_yen(img)
th4 = img.copy() > threshold_li(img)
'''

h = histogram(img)

g = gaussian(img, 600)

binary = img.copy()
binary[binary < 110] = 0
binary[binary > 0] = 1

'''
plt.figure()
plt.subplot(321)
plt.imshow(img, cmap="gray")
plt.subplot(322)
plt.imshow(binary, cmap="gray")
plt.subplot(323)
plt.imshow(th1, cmap="gray")
plt.subplot(324)
plt.imshow(th2, cmap="gray")
plt.subplot(325)
plt.imshow(th3, cmap="gray")
plt.subplot(326)
plt.imshow(th4, cmap="gray")
'''

plt.figure()
plt.subplot(121)
plt.imshow(img, cmap="gray")
plt.subplot(122)
plt.imshow((img/g), cmap="gray")

'''
plt.figure()
plt.plot(h[1], h[0])
'''

plt.show()
