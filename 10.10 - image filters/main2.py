import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops
from collections import defaultdict


def area(LB, label):
    return (LB == label).sum()

def centroid(LB, label):
    pos = np.where(LB==label)
    cy = np.mean(pos[0])
    cx = np.mean(pos[1])
    return cy, cx

def neighbours4(y, x):
    return (y, x+1),(y+1, x), (y, x-1), (y-1, x)


def neighboursX(y, x):
    return (y-1, x+1),(y+1, x+1),(y+1, x-1), (y-1, x-1)


def neighbours8(y, x):
    return neighbours4(y, x) + neighboursX(y, x)
    
def boundaries(LB, label=1, connectivity=neighbours4):
    bounds = []
    pos = np.where(LB==label)
    for y, x in zip(*pos):
        for yn, xn in connectivity(y,x):
            if yn < 0 or yn > LB.shape[0]-1:
                bounds.append((y,x))
            elif xn<0 or xn>LB.shape[1]-1:
                bounds.append((y,x))
            elif LB[yn, xn] != label:
                bounds.append((y,x))
    return bounds

def perimeter(LB, label=1, connectivity=neighbours4):
    return len(boundaries(LB, label, connectivity))

def is_circle(per, area):
    return ((per**per)/area)

def distance(px1, px2):
    return ((px1[0]-px2[0])**2 + (px1[1]-px2[1])**2) **0.5

def radial_distance(LB, label, connectivity):
    r, c = centroid(LB, label)
    bounds = boundaries(LB, label, connectivity)
    K = len(bounds)
    rd = 0
    for rk, ck in bounds:
        rd += distance((r,c), (rk,ck))
    return rd / K

def std_radial(LB, label, connectivity):
    r, c = centroid(LB, label)
    bounds = boundaries(LB, label, connectivity)
    K = len(bounds)
    sr =0
    rd = radial_distance(LB, label, connectivity)
    for rk, ck in bounds:
        sr += (distance((r,c), (rk,ck))-rd) **2
    return(sr /K) **0.5

def circularity_std(LB, label=1, connectivity=neighbours4):
    return (radial_distance(LB, label, connectivity)/
            std_radial(LB, label, connectivity))

LB = np.zeros((16, 16))
LB[4:, :4] = 2

LB[3:10, 8:] = 1
LB[[3, 4, 3],[8, 8, 9]] = 0
LB[[8, 9, 9],[8, 8, 9]] = 0
LB[[3, 4, 3],[-2, -1, -1]] = 0
LB[[9, 8, 9],[-2, -1, -1]] = 0

LB[12:-1, 6:9] = 3
data = np.load("ellipses.npy")

lb = label(data)

plt.imshow(lb)
'''
nominals = np.array([1, 2, 5, 10])
areas = defaultdict(lambda: 0)

for lbl in range(1, lb.max()+1):
    areas[area(lb, lbl)] += 1

result = (np.array([areas[k] for k in sorted(areas)] 
                * nominals)).sum()
print(result)

for lbl in range(1, int(LB.max()+1)):
    print(f"4n = {perimeter(LB, lbl)}")
for  lbl in range(1, int(LB.max()+1)):
    print(f"8n = {perimeter(LB, lbl, neighbours8)}")

for lbl in range(1, int(LB.max()+1)):
    print(f"4n = {circularity_std(LB, lbl)}")
for  lbl in range(1, int(LB.max()+1)):
    print(f"8n = {circularity_std(LB, lbl, neighbours8)}")


for lbl in range(1, int(lb.max()+1)):
    print(f"4n {lbl} = {circularity_std(lb, lbl)}")
    
for  lbl in range(1, int(lb.max()+1)):
    print(f"8n {lbl} = {circularity_std(lb, lbl, neighbours8)}")
'''

circles = 0

for reg in regionprops(lb):
    print(f"{reg.label} = {reg.eccentricity}")    
    if reg.eccentricity == 0.0:
        circles +=1
print(circles)



'''
plt.imshow(LB)
cc1 = centroid(LB, 1)
print(perimeter(LB, 1, neighbours4))
plt.scatter(cc1[1], cc1[0])

cc2 = centroid(LB, 2)
print(perimeter(LB, 2, neighbours4))
plt.scatter(cc2[1], cc2[0])

cc3 = centroid(LB, 3)
print(perimeter(LB, 3, neighbours4))
plt.scatter(cc3[1], cc3[0])

props = regionprops(LB.astype(int))
print(props[0].area)
'''
