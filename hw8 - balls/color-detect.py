import cv2
import numpy as np

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
#cv2.namedWindow("Camera", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)
position = [0, 0]

def on_mouse_click(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        global position
        position = [y, x]

cv2.setMouseCallback("Camera", on_mouse_click)

measures =[]
hsv = []
while cam.isOpened():
    ret, frame = cam.read()
    
    pixel = frame[position[0], position[1], :]
    measures.append(pixel)
    if len(measures) == 10:
        bgr = np.uint8([[np.mean(measures,0)]])
        hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
        
        measures.clear()
    cv2.putText(frame, f"HSV = {hsv}",(10,30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,255,127))    
    cv2.circle(frame, position[::-1], 5, (0,117,255), 2)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    cv2.imshow("Camera", frame)
cam.release()
cv2.destroyAllWindows()
