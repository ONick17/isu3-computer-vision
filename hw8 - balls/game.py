import cv2
import numpy as np
import random
import time

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
#cv2.namedWindow("Camera", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)

colors = ["red", "yellow", "green", "blue"]
guess = random.sample(colors, 3)
print(guess)
points = 0
flip = True

blue_l = np.array([90, 100, 130])
blue_u = np.array([110, 240, 210])

red_l = np.array([170, 140, 110])
red_u = np.array([190, 250, 210])
red2_l = np.array([0, 140, 110])
red2_u = np.array([1, 250, 210])

green_l = np.array([60, 120, 80])
green_u = np.array([80, 200, 180])

yellow_l = np.array([20, 90, 140])
yellow_u = np.array([35, 190, 220])

def find_circles(frame, mask, color=(0,255,255)):
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius >10:
            cv2.circle(frame, (int(x), int(y)), int(radius), color, 2)
        return(int(x))
    return None

while cam.isOpened():
    ret, frame = cam.read()
    if flip:
        frame = cv2.flip(frame, 1)
    blured = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blured = cv2.GaussianBlur(blured, (11, 11), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    blue_mask = cv2.inRange(hsv, blue_l, blue_u)
    blue_mask = cv2.dilate(blue_mask, None, iterations=4)
    blue_x = find_circles(frame, blue_mask, (255, 0, 0))

    red_mask = cv2.inRange(hsv, red_l, red_u)
    red2_mask = cv2.inRange(hsv, red2_l, red2_u)
    red_mask = red_mask + red2_mask
    red_mask = cv2.dilate(red_mask, None, iterations=4)
    red_x = find_circles(frame, red_mask, (0, 0, 255))

    green_mask = cv2.inRange(hsv, green_l, green_u)
    green_mask = cv2.dilate(green_mask, None, iterations=4)
    green_x = find_circles(frame, green_mask, (0, 255, 0))

    yellow_mask = cv2.inRange(hsv, yellow_l, yellow_u)
    yellow_mask = cv2.dilate(yellow_mask, None, iterations=4)
    yellow_x = find_circles(frame, yellow_mask) 
    
    #mask = blue_mask + red_mask + green_mask + yellow_mask
    #mask = cv2.bitwise_and(frame, frame, mask = mask)
    #cv2.imshow("Mask", mask)

    cv2.putText(img=frame, text='points: '+str(points), org=(50, 50), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1, color=(170, 17, 117), thickness=2)
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    elif key == ord('f'):
        flip = not flip
    elif key == ord(' '):
        keke = {}
        if blue_x != None:
            keke["blue"] = blue_x
        if red_x != None:
            keke["red"] = red_x
        if green_x != None:
            keke["green"] = green_x
        if yellow_x != None:
            keke["yellow"] = yellow_x
        ans = []
        for i in sorted(keke, key=keke.get):
            ans.append(i)
        if ans == guess:
            points += 1
            cv2.putText(img=frame, text='correct', org=(150, 250), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=3, color=(0, 255, 0), thickness=3)
        else:
            points -= 1
            cv2.putText(img=frame, text='wrong', org=(150, 250), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=3, color=(0, 0, 255), thickness=3)
        cv2.imshow("Camera", frame)
        key = cv2.waitKey(1)
        time.sleep(1)
        guess = random.sample(colors, 3)
        print(guess)
cam.release()
cv2.destroyAllWindows()
