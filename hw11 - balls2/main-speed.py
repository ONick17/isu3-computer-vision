import cv2
import numpy as np
import random
import time

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cam = cv2.VideoCapture(0)
ball_size = 0.07
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)
print(cam.get(cv2.CAP_PROP_FPS))
'''
blue_l = np.array([90, 100, 130])
blue_u = np.array([110, 240, 210])

red_l = np.array([170, 140, 110])
red_u = np.array([190, 250, 210])
red2_l = np.array([0, 140, 110])
red2_u = np.array([1, 250, 210])

yellow_l = np.array([20, 90, 140])
yellow_u = np.array([35, 190, 220])
'''
green_l = np.array([60, 70, 80])
green_u = np.array([80, 200, 250])

flip = True

def find_circle(frame, mask, color=(0,255,255)):
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius >10:
            cv2.circle(frame, (int(x), int(y)), int(radius), color, 2)
        return([int(x), int(y), int(radius), time.time()])
    return None

ans = 'none'
ball = False
ball_old = False
cnt = 0

while cam.isOpened():
    ret, frame = cam.read()

    if flip:
        frame = cv2.flip(frame, 1)

    blured = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blured = cv2.GaussianBlur(blured, (11, 11), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    '''
    blue_mask = cv2.inRange(hsv, blue_l, blue_u)
    blue_mask = cv2.dilate(blue_mask, None, iterations=4)
    red_mask = cv2.inRange(hsv, red_l, red_u)
    red2_mask = cv2.inRange(hsv, red2_l, red2_u)
    red_mask = red_mask + red2_mask
    red_mask = cv2.dilate(red_mask, None, iterations=4)
    yellow_mask = cv2.inRange(hsv, yellow_l, yellow_u)
    yellow_mask = cv2.dilate(yellow_mask, None, iterations=4)
    mask = blue_mask + red_mask + green_mask + yellow_mask
    '''
    green_mask = cv2.inRange(hsv, green_l, green_u)
    green_mask = cv2.dilate(green_mask, None, iterations=4)

    '''
    blue_x = find_circle(frame, blue_mask, (255, 0, 0))
    red_x = find_circle(frame, red_mask, (0, 0, 255))
    yellow_x = find_circle(frame, yellow_mask)
    mask = cv2.bitwise_and(frame, frame, mask = mask)
    '''
    ball = find_circle(frame, green_mask, (0, 255, 0))
    if ball and ball_old:
        del_sec = 1/cam.get(cv2.CAP_PROP_FPS)
        dist_x = ((ball_old[0] - ball[0])**2 + (ball_old[1] - ball[1])**2) ** 0.5
        del_dist = ball_size / (ball_old[2]*2)
        speed_x = dist_x * del_dist / (ball[3]-ball_old[3])
        speed_z = abs(ball_old[2]*2 - ball[2]*2)*ball_size / (ball[3]-ball_old[3])
        ans = round(speed_x+speed_z, 2)
    if ball:
        ball_old = ball
    else:
        ans = "none"
        ball_old = False
    
    mask = cv2.bitwise_and(frame, frame, mask = green_mask)
    
    cv2.imshow("Mask", mask)
    cv2.putText(img=frame, text=str(ans), org=(50, 50), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1, color=(0, 0, 255), thickness=2)
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    elif key == ord('f'):
        flip = not flip
cam.release()
cv2.destroyAllWindows()
