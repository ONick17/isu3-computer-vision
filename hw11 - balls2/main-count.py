import cv2
import numpy as np
from skimage.measure import label


video = cv2.VideoCapture("balls.mp4")
#cv2.namedWindow("Thresh", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("Video", cv2.WINDOW_KEEPRATIO)
mask = np.ones((5,5))

while video.isOpened():
    ret, frame = video.read()
    if ret:
        converted = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(converted, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)
        thresh = cv2.erode(thresh, mask, iterations=1)
        thresh = cv2.dilate(thresh, mask, iterations=1)
        cnt = 0
        for i in range(1, label(thresh).max()+1):
            contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
            if len(contours)> 0:
                for i in contours:
                    (x,y), radius = cv2.minEnclosingCircle(i)
                    if radius > 10:
                        cnt += 1
                        cv2.circle(frame, (int(x), int(y)), int(radius), (0,255, 0), 2 )
        cnt = int(cnt ** 0.5)
        cv2.putText(img=frame, text=str(cnt), org=(50, 50), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1, color=(0, 0, 255), thickness=2)
        #cv2.imshow("Thresh", thresh)
        cv2.imshow("Video", frame)
        key = cv2.waitKey(1)
        if key == ord('q'):
            break
    else:
        break

video.release()
cv2.destroyAllWindows()
