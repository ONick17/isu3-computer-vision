import cv2
import numpy as np
import matplotlib.pyplot as plt


def find_circles(frame, mask, color=(0,255,255)):
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius >10:
            cv2.circle(frame, (int(x), int(y)), int(radius), color, 2)
        return(int(x))
    return None


print("Start")
#blue_l = np.array([100, 80, 110])
#blue_u = np.array([110, 140, 160])
red_l = np.array([0, 90, 160])
red_u = np.array([10, 155, 210])
green_l = np.array([70, 90, 100])
green_u = np.array([90, 170, 150])

ans = []
#cnt = 0
cam = cv2.VideoCapture("video.mp4")
success, frame = cam.read()
print("Analyzing video...")
while success:
    #cv2.imwrite("frame%d.jpg" % cnt, img)
    #cnt += 1
    blured = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blured = cv2.GaussianBlur(blured, (11, 11), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    #blue_mask = cv2.inRange(hsv, blue_l, blue_u)
    #blue_mask = cv2.dilate(blue_mask, None, iterations=4)
    #blue_x = find_circles(frame, blue_mask, (255, 0, 0))
    red_mask = cv2.inRange(hsv, red_l, red_u)
    red_mask = cv2.dilate(red_mask, None, iterations=4)
    red_x = find_circles(frame, red_mask, (0, 0, 255))
    green_mask = cv2.inRange(hsv, green_l, green_u)
    green_mask = cv2.dilate(green_mask, None, iterations=4)
    green_x = find_circles(frame, green_mask, (0, 255, 0))
        
    if (green_x != None) and (red_x != None):
        ans.append(frame)
    success, frame = cam.read()

print("Video was analyzed")
print("Launching player, press q to finish")
cnt = -1
while cam.isOpened():
    cnt += 1
    if cnt >= len(ans):
        cnt = 0
    cv2.imshow("Camera", ans[cnt])
    key = cv2.waitKey(25)
    if key == ord('q'):
        break

print("The end")
cam.release()
cv2.destroyAllWindows()
