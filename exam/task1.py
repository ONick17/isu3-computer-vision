import cv2
from skimage import color
from skimage.measure import label, regionprops
import numpy as np

img_or = cv2.imread("task1.png")
#cv2.imshow("task1-original", img)
img = color.rgb2hsv(img_or)[:, :, 0]
#cv2.imshow("task1-hsv", img)

back_color = img[0][0]
colors = []
for i in img:
    for j in i:
        if (j != back_color) and (j not in colors):
            colors.append(j)
colors_imgs = []
for i in range(len(colors)):
    keke = img.copy()
    #keke[keke != colors[i]] = 0
    keke[keke == colors[i]] = 1
    colors[i] = keke
    #cv2.imshow(str(i), keke)

all_props = []
max_area = 0
cnt = 0
one_label = np.array([[0]*200]*200)
for i in colors:
    cnt += 1
    labl = label(i)
    props = regionprops(labl)
    one_label += (labl * cnt)
    for j in props:
        all_props.append(j)
        area = j.area - j.perimeter
        if area > max_area:
            max_area = area
            max_prop = j

img = img_or.copy()
for i in max_prop.coords:
    img[i[0]][i[1]] = [0, 0, 255]

img_or = cv2.resize(img_or, (600, 600), interpolation = cv2.INTER_AREA)
img = cv2.resize(img, (600, 600), interpolation = cv2.INTER_AREA)
cv2.putText(img=img, text=str(int(max_area)), org=(30, 30), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1, color=(0, 0, 255), thickness=1)
cv2.imshow("task1-original", img_or)
cv2.imshow("task1-answer", img)

import matplotlib.pyplot as plt
plt.imshow(one_label)
plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()



