import cv2
import pyautogui
import numpy as np
import time
from mss import mss

pyautogui.PAUSE = 0
x_begin = 100
x_end = 850
y_begin = 350
y_end = 450

y = 80
x_l_const = 120
x_delta = 90
x_r_const = x_l_const + x_delta

move = 2.1
start = time.time()

def check1():
    screen = np.array(pyautogui.screenshot())
    screen = cv2.line(screen, (x_begin, y_begin),
                              (x_end, y_begin), (10,255,0), 2)
    screen = cv2.line(screen, (x_begin, y_end),
                              (x_end, y_end), (10,255,0), 2)
    screen = cv2.line(screen, (x_begin, y_begin),
                              (x_begin, y_end), (10,255,0), 2)
    screen = cv2.line(screen, (x_end, y_begin),
                              (x_end, y_end), (10,255,0), 2)
    cv2.imshow('screenshot', screen)
    cv2.waitKey()
    cv2.destroyAllWindows()

def check2():
    screen = np.array(pyautogui.screenshot())
    screen = screen[y_begin:y_end, x_begin:x_end]
    screen = cv2.line(screen, (x_l, y),(x_r, y), (10,255,0), 1)
    cv2.imshow('screenshot', screen)
    cv2.waitKey()
    cv2.destroyAllWindows()

def main2():
    #cv2.namedWindow("T-Rex", cv2.WINDOW_KEEPRATIO)
    screen_mss = mss()
    sizes = {'top':y_begin, 'left':x_begin, 'width':x_end-x_begin, 'height':y_end-y_begin} 
    while True:
        delta = int((time.time()-start)//move)
        x_l = x_l_const + delta
        x_r = x_r_const + delta
        #screen = np.array(pyautogui.screenshot())
        #screen = screen[y_begin:y_end, x_begin:x_end]
        screen = np.array(screen_mss.grab(sizes))
        #screen = cv2.cvtColor(screen, cv2.COLOR_BGRA2GRAY)
        screen = cv2.line(screen, (x_l, y-1),(x_r, y-1), (10,255,0), 1)
        cv2.putText(img=screen, text=str(x_l)+' '+str(x_r), org=(50, 50), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=1, color=(0, 0, 255), thickness=2)
        print(screen[y, x_l:x_r])
        if 83 in screen[y, x_l:x_r]:
            pyautogui.keyUp('down')
            pyautogui.keyDown('up')
        else:q
            pyautogui.keyUp('up')
            pyautogui.keyDown('down')
            print(time.time() - start)

        cv2.imshow("T-Rex", screen)
        key = cv2.waitKey(1)
        if key == ord('q'):
            break
    cv2.destroyAllWindows()

#check1()
#check2()
main2()
